package cbn.apps.sahabat24;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;

import cbn.apps.sahabat24.lib.RealmController;
import cbn.apps.sahabat24.lib.User;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    Realm realm;
    User userData;
    SignInButton btnGoogle;
    TextView txtSkip;
    Button btnSignIn, btnEmail;
    EditText edtLoginEmail, edtLoginPassword;
    LoginButton btnFacebook;
    CallbackManager callbackManager;
    GoogleSignInOptions gso;
    GoogleApiClient googleApiClient;
    private static final int RC_SIGN_IN = 9001;

    private static final String LOGIN_URL = "https://www.jawaban.com/api/user.php?m=login";
    private static final String LOGIN_FB = "https://www.jawaban.com/api/user.php?m=loginfb";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();
        btnEmail = (Button) findViewById(R.id.btnEmail);
        edtLoginEmail = (EditText) findViewById(R.id.loginEmail);
        edtLoginPassword = (EditText) findViewById(R.id.loginPassword);
        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        txtSkip = (TextView) findViewById(R.id.txtSkip);
        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }
        });

        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                finish();
            }
        });

        FacebookSdk.sdkInitialize(getApplicationContext());
        //LoginManager.getInstance().logOut();

        callbackManager = CallbackManager.Factory.create();

        /*(if (isLoggedIn()) {
            String userId = Profile.getCurrentProfile().getId();
            String userName = Profile.getCurrentProfile().getName();
            Uri image = Profile.getCurrentProfile().getProfilePictureUri(100, 100);
        } else {
            txtSkip.setText("Not Login");
        }
        */
        //Facebook Handler
        btnFacebook = (LoginButton) findViewById(R.id.btnfb);
        btnFacebook.setReadPermissions("email");

        btnFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String userId = loginResult.getAccessToken().getUserId();
                //txtSkip.setText(userId);
                loginSocialMedia(userId, "", "", "FB");
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                //txtSkip.setText(error.getMessage());
            }
        });


        //Google Handler
        btnGoogle = (SignInButton) findViewById(R.id.btnGmail);
        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });


        //Realm
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
        this.realm = RealmController.with(this).getRealm();


        if (RealmController.with(this).hasUser()) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        } else {
            setUserData();
            RealmController.with(this).refresh();
        }
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = edtLoginEmail.getText().toString().trim().toLowerCase();
                String password = edtLoginPassword.getText().toString().trim();

                if (email.isEmpty()) {
                    edtLoginEmail.setError("Email can't be empty");
                    return;
                }
                if (password.isEmpty()) {
                    edtLoginPassword.setError("Please fill password");
                    return;
                }
                loginEmail(email, password);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void loginEmail(String txEmail, String txPassword) {
        class RegisterUser extends AsyncTask<String, Void, String> {
            ProgressDialog loading;
            cbn.apps.sahabat24.lib.RegisterUser ruc = new cbn.apps.sahabat24.lib.RegisterUser();
            String userName;
            String fullName;
            String email;

            boolean result = false;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(LoginActivity.this, "Please Wait", null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                JsonObject jsonObject = new JsonParser().parse(s).getAsJsonObject();
                String error = jsonObject.get("error").getAsString();
                if (error.equals("false")) {
                    this.result = true;
                    JsonObject jsonObjectRes = jsonObject.get("message").getAsJsonObject();
                    userName = jsonObjectRes.get("uid").getAsString();
                    fullName = jsonObjectRes.get("name").getAsString();
                    email = jsonObjectRes.get("email").getAsString();
                    LoginActivity.this.registerRealm(userName, fullName, email, "email");
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                } else {
                    this.result = false;
                    Toast.makeText(LoginActivity.this, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                }
                loading.dismiss();
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<String, String>();
                data.put("email", params[0]);
                data.put("password", params[1]);

                String result = ruc.sendPostRequest(LOGIN_URL, data);

                return result;
            }
        }

        RegisterUser ru = new RegisterUser();
        ru.execute(txEmail, txPassword);
    }

    private void loginSocialMedia(final String txUserId, final String txUserName, final String txEmail, final String type) {
        class RegisterUser extends AsyncTask<String, Void, String> {
            ProgressDialog loading;
            cbn.apps.sahabat24.lib.RegisterUser ruc = new cbn.apps.sahabat24.lib.RegisterUser();
            String userName = txUserName;
            String userId = txUserId;
            String email = txEmail;

            boolean result = false;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(LoginActivity.this, "Please Wait", null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                JsonObject jsonObject = new JsonParser().parse(s).getAsJsonObject();
                String error = jsonObject.get("error").getAsString();
                if (error.equals("false")) {
                    this.result = true;
                    if (type.equals("facebook")) {
                        userName = Profile.getCurrentProfile().getName();
                        userId = Profile.getCurrentProfile().getId();
                    }
                    LoginActivity.this.registerRealm(userId, userName, email, "email");
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                } else {
                    this.result = false;
                    Toast.makeText(LoginActivity.this, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                }
                loading.dismiss();
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<String, String>();
                data.put("uid", params[0]);
                String result = ruc.sendPostRequest(LOGIN_FB, data);

                return result;
            }
        }

        RegisterUser ru = new RegisterUser();
        ru.execute(txUserId);
    }

    private void registerRealm(String txUserName, String txFullName, String txEmail, String type) {
        //Database Realm
        User user = new User();
        user.setId((int) (RealmController.getInstance().getUsers().size() + System.currentTimeMillis()));
        user.setUserName(txUserName);
        user.setFullName(txFullName);
        user.setEmail(txEmail);
        user.setType(type);
        // Persist your data easily
        realm.beginTransaction();
        realm.copyToRealm(user);
        realm.commitTransaction();
    }


    private void setUserData() {
        RealmController.with(this).refresh();
        if (RealmController.with(this).hasUser()) {
            userData = RealmController.with(this).getUser();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("GAPI", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.d("GAPIUSER", "UserName:" + acct.getEmail());
            loginSocialMedia(acct.getId(), acct.getDisplayName(), acct.getEmail(), "gmail");
        } else {
            // Signed out, show unauthenticated UI.

        }
    }
}
