package cbn.apps.sahabat24;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;

import static cbn.apps.sahabat24.R.layout.activity_register;

public class RegisterActivity extends AppCompatActivity {

    AutoCompleteTextView userName;
    AutoCompleteTextView fullName;
    AutoCompleteTextView email;
    EditText password;
    EditText repassword;
    Button btnSignUp;
    /*
        @BindView(R.id.userName)
        @BindView(R.id.fullName)
        @BindView(R.id.email)
        @BindView(R.id.password)
        @BindView(R.id.repassword)
        @BindView(R.id.btnSignUp)
        @BindView(R.id.txtLogin)
        */
    TextView txtLogin;
    private static final String REGISTER_URL = "https://www.jawaban.com/api/user.php?m=register";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_register);
        txtLogin = (TextView) findViewById(R.id.txtLogin);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        userName = (AutoCompleteTextView) findViewById(R.id.userName);
        fullName = (AutoCompleteTextView) findViewById(R.id.fullName);
        email = (AutoCompleteTextView) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        repassword = (EditText) findViewById(R.id.repassword);
        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sUserName = userName.getText().toString().trim();
                String sFullName = fullName.getText().toString().trim();
                String sEmail = email.getText().toString().trim().toLowerCase();
                String sPassword = password.getText().toString().trim();
                String sRePassword = repassword.getText().toString().trim();


                if (sFullName.isEmpty() || sEmail.isEmpty() || sPassword.isEmpty() || sRePassword.isEmpty()) {
                    Toast.makeText(RegisterActivity.this, "Please Complete All Field to proceed Register", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (sPassword.equals(sRePassword)) {
                    registerOnline(sUserName, sFullName, sEmail, sPassword);
                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                    builder.setMessage("Thankyou for registering. Your Account has created. Please Login with your credential");
                    // Add the buttons
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button
                            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                            finish();

                        }
                    });

                    // Create the AlertDialog
                    AlertDialog dialog = builder.create();
                    dialog.show();

                } else {
                    password.setError("Password not match");
                    Toast.makeText(RegisterActivity.this, "Password and Repassword not Match", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });

    }

    private void registerOnline(String txUserName, String txFullName, String txEmail, String txPassword) {
        class RegisterUser extends AsyncTask<String, Void, String> {
            ProgressDialog loading;
            cbn.apps.sahabat24.lib.RegisterUser ruc = new cbn.apps.sahabat24.lib.RegisterUser();

            boolean result = true;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(RegisterActivity.this, "Please Wait", null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                JsonObject jsonObject = new JsonParser().parse(s).getAsJsonObject();
                String error = jsonObject.get("error").getAsString();
                if (error.equals("true")) {
                    this.result = false;
                    Toast.makeText(RegisterActivity.this, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<String, String>();
                data.put("userid", params[0]);
                data.put("name", params[1]);
                data.put("email", params[2]);
                data.put("password", params[3]);

                String result = ruc.sendPostRequest(REGISTER_URL, data);

                return result;
            }
        }

        RegisterUser ru = new RegisterUser();
        ru.execute(txUserName, txFullName, txEmail, txPassword);

    }
}
