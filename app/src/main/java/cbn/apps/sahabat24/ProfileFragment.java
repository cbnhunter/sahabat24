package cbn.apps.sahabat24;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.Profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import cbn.apps.sahabat24.lib.ImageLoader;
import cbn.apps.sahabat24.lib.JSONfunctions;
import cbn.apps.sahabat24.lib.RealmController;
import cbn.apps.sahabat24.lib.User;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button btnLogout;
    TextView profileusername, bio, join_date;
    User userData;
    String username;
    HashMap<String, String> profileResult;
    ImageLoader imageLoader;
    ImageView userImage, userImageCover;
    boolean loginFB;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        btnLogout = (Button) view.findViewById(R.id.btnLogout);
        profileusername = (TextView) view.findViewById(R.id.profileusername);
        imageLoader = new ImageLoader(getContext());
        userData = RealmController.with(this).getUser();
        userImage = (ImageView) view.findViewById(R.id.profile_image);
        userImageCover = (ImageView) view.findViewById(R.id.userImageCover);
        username = userData.getUserName();
        profileusername.setText(userData.getFullName());
        join_date = (TextView) view.findViewById(R.id.txtJoinDate);
        bio = (TextView) view.findViewById(R.id.txtBio);

        FacebookSdk.sdkInitialize(getContext());
        //LoginManager.getInstance().logOut();

        CallbackManager callbackManager = CallbackManager.Factory.create();

        if (isLoggedIn()) {
            loginFB = true;
        } else {
            loginFB = false;
        }

        new GetUserProfile().execute();

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearUserData();
            }
        });

        return view;
    }

    private void clearUserData() {
        RealmController.with(this).clearAll();
        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }

    private class GetUserProfile extends AsyncTask<Void, Void, Void> {
        JSONObject jsonObject;
        JSONArray jsonArray;
        ProgressDialog loading;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = ProgressDialog.show(getActivity(), "Please Wait", null, true, true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Create an array
            profileResult = new HashMap<String, String>();
            // Retrieve JSON Objects from the given URL address
            jsonObject = JSONfunctions
                    .getJSONfromURL("https://www.jawaban.com/api/user.php?m=getprofile&userid=" + username);

            try {
                // Locate the array name in JSON
                jsonObject = jsonObject.getJSONObject("message");
                // Retrive JSON Objects
                profileResult.put("bio", jsonObject.getString("bio"));
                profileResult.put("tgl_join", jsonObject.getString("tgl_join"));
                profileResult.put("user_image", jsonObject.getString("user_image"));
                profileResult.put("user_cover_image", jsonObject.getString("user_cover_image"));
                profileResult.put("total_ask", jsonObject.getString("total_ask"));
                profileResult.put("total_ans", jsonObject.getString("total_ans"));

            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            bio.setText(profileResult.get("bio"));
            join_date.setText(profileResult.get("tgl_join"));
            if (loginFB) {
                Uri image = Profile.getCurrentProfile().getProfilePictureUri(400, 400);
                imageLoader.DisplayImage(image.toString(), userImage);
                imageLoader.DisplayImage(image.toString(), userImageCover);
            } else {
                imageLoader.DisplayImage(profileResult.get("user_image"), userImage);
                imageLoader.DisplayImage(profileResult.get("user_cover_image"), userImageCover);
            }
            loading.dismiss();
        }
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }
}
