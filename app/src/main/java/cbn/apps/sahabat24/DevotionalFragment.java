package cbn.apps.sahabat24;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import cbn.apps.sahabat24.lib.ImageLoader;
import cbn.apps.sahabat24.lib.JSONfunctions;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link DevotionalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DevotionalFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ImageLoader imageLoader;
    TextView txtTitle, txtReleaseDate;
    HtmlTextView txtDetail;
    ImageView imgDevo;
    ProgressBar progLoadDetailDevo;
    ScrollView contentView;
    String id = null;
    String category = null;
    String detail = null;
    String title = null;
    String releaseDate = null;
    String image = null;
    String url = null;


    public DevotionalFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DevotionalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DevotionalFragment newInstance(String param1, String param2) {
        DevotionalFragment fragment = new DevotionalFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_devotional, container, false);
        imageLoader = new ImageLoader(getContext());
        //imageLoader.clearCache();
        txtTitle = (TextView) view.findViewById(R.id.txtDevoTitle);
        txtDetail = (HtmlTextView) view.findViewById(R.id.txtDevoDetail);
        txtReleaseDate = (TextView) view.findViewById(R.id.txtDevoReleaseDate);
        imgDevo = (ImageView) view.findViewById(R.id.imgMainDevo);
        progLoadDetailDevo = (ProgressBar) view.findViewById(R.id.progLoadDetailDevo);
        contentView = (ScrollView) view.findViewById(R.id.devoContent);
        new GetDevotional().execute();
        return view;
    }
    private class GetDevotional extends AsyncTask<Void, Void, Void> {
        JSONObject jsonArticle;
        JSONArray arrayArticle;

        @Override
        protected void onPreExecute() {
            contentView.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Retrieve JSON Objects from the given URL address
            jsonArticle = JSONfunctions
                    .getJSONfromURL("http://www.jawaban.com/api/index.php?m=gdd");

            try {
                // Locate the array name in JSON
                arrayArticle = jsonArticle.getJSONArray("article");

                for (int i = 0; i < arrayArticle.length(); i++) {
                    jsonArticle = arrayArticle.getJSONObject(i);
                    // Retrive JSON Objects
                    id = jsonArticle.getString("id");
                    title = jsonArticle.getString("title");
                    category = jsonArticle.getString("category");
                    detail = jsonArticle.getString("content");
                    releaseDate = jsonArticle.getString("release_date");
                    image = jsonArticle.getString("image");
                    url = jsonArticle.getString("url");

                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            // Pass the results into ListViewAdapter.java
            contentView.setVisibility(View.VISIBLE);

            String s = null;
//            try {
//                s = URLDecoder.decode(detail, "UTF-8");
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
            txtTitle.setText(title);
            txtReleaseDate.setText(releaseDate);
            txtDetail.setHtml(detail);
            imageLoader.DisplayImage(image, imgDevo);
            progLoadDetailDevo.setVisibility(View.GONE);
        }
    }
}
